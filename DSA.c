#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_NAME_LENGTH 20 // 30 characters + null terminator
#define DATE_LENGTH 10   // 10 characters (YYYY-MM-DD) + null terminator
#define REGISTRATION_LENGTH 6// 6 characters + null terminator
#define MAX_STUDENTS 100 // maximum number of students in an array

struct Student {
    char name[MAX_NAME_LENGTH];
    char dob[DATE_LENGTH];
    char registration[REGISTRATION_LENGTH];
    char program_code[4];
    float tuition;

};


void createStudent(struct Student *students, int *numStudents) {
    if (*numStudents >= MAX_STUDENTS) {
        printf("Maximum number of students reached!\n");
        return;
    }

    struct Student newStudent;

    printf("Enter student name (no spaces) :  ");
    scanf("%20s", newStudent.name);
    printf("Enter date of birth (YYYY-MM-DD):  ");
    scanf("%12s", newStudent.dob);

    printf("Enter registration number :  ");
    scanf("%6s", newStudent.registration);

    printf("Enter program code :  ");
    scanf("%4s", newStudent.program_code);

    printf("Enter annual tuition:  ");
    scanf("%f", &newStudent.tuition);

    students[*numStudents] = newStudent;
    (*numStudents)++;
}